package com.flatstack.vk.test;

import android.util.SparseArray;
import com.flatstack.vk.test.data.Group;
import com.flatstack.vk.test.data.Photo;
import com.flatstack.vk.test.data.Post;
import com.flatstack.vk.test.data.Profile;
import com.google.gson.Gson;
import com.vk.sdk.api.VKResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class PostsParser {

    private JSONObject mJsonRespone;

    public PostsParser(VKResponse vkResponse) throws JSONException{
        mJsonRespone = vkResponse.json.getJSONObject("response");
    }

    public String getNextFrom() throws JSONException {
        return mJsonRespone.getString("next_from");
    }

    public ArrayList<Post> getPosts() throws JSONException {
        JSONArray items = mJsonRespone.getJSONArray("items");
        SparseArray<Group> groups = parseGroups(mJsonRespone.getJSONArray("groups"));
        SparseArray<Profile> profiles = parseProfiles(mJsonRespone.getJSONArray("profiles"));
        ArrayList<Post> posts = parsePosts(items);
        for (Post p : posts) {
            if (p.isGroupPost()) {
                p.setGroup(groups.get(p.getAbsSourceId()));
            } else {
                p.setProfile(profiles.get(p.getAbsSourceId()));
            }
        }
        return posts;
    }

    private ArrayList<Post> parsePosts(JSONArray items) throws JSONException {
        Gson gson = new Gson();
        ArrayList<Post> postArray = new ArrayList<Post>();
        nextPost: for (int i = 0; i < items.length(); i++) {
            Post post = new Post();
            JSONObject item = items.getJSONObject(i);
            if (item.has("copy_history")) continue;
            post.setText(item.getString("text"));
            post.setDate(item.getLong("date"));
            if (item.has("likes")) {
                JSONObject likes = item.getJSONObject("likes");
                post.setLikesCount(likes.getInt("count"));
            }
            if (item.has("reposts")) {
                JSONObject reposts = item.getJSONObject("reposts");
                post.setRepostsCount(reposts.getInt("count"));
            }
            post.setSourceId(item.getInt("source_id"));
            if (item.has("attachments")) {
                JSONArray attachments = item.getJSONArray("attachments");
                ArrayList<Photo> photoArray = new ArrayList<Photo>();
                for (int j = 0; j < attachments.length(); j++) {
                    JSONObject attach = attachments.getJSONObject(j);
                    String type = attach.getString("type");
                    if (type.equals("photo")) {
                        Photo photo = gson.fromJson(attach.getJSONObject("photo").toString(), Photo.class);
                        photoArray.add(photo);
                    } else {
                        continue nextPost;
                    }
                }
                post.setPhotos(photoArray);
            }
            postArray.add(post);
        }
        return postArray;
    }

    private SparseArray<Group> parseGroups(JSONArray groups) throws JSONException {
        Gson gson = new Gson();
        SparseArray<Group> groupArray = new SparseArray<Group>();
        for (int i = 0; i < groups.length(); i++) {
            JSONObject item = groups.getJSONObject(i);
            Group group = gson.fromJson(item.toString(), Group.class);
            groupArray.append(group.getGroupId(), group);
        }
        return groupArray;
    }

    private SparseArray<Profile> parseProfiles(JSONArray profiles) throws JSONException {
        Gson gson = new Gson();
        SparseArray<Profile> profileArray = new SparseArray<Profile>();
        for (int i = 0; i < profiles.length(); i++) {
            JSONObject item = profiles.getJSONObject(i);
            Profile profile = gson.fromJson(item.toString(), Profile.class);
            profileArray.append(profile.getProfileId(), profile);
        }
        return profileArray;
    }
}
