package com.flatstack.vk.test;

import com.squareup.otto.Bus;

/**
 * Created by Vitaly Kirillov on 16.06.2014.
 */
public final class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {}
}