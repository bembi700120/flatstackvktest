package com.flatstack.vk.test.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public class Profile {

    @SerializedName("id")
    private int mId;

    @SerializedName("first_name")
    private String mFirstName;

    @SerializedName("last_name")
    private String mLastName;

    @SerializedName("photo_100")
    private String mPhoto100;

    public int getProfileId() {
        return mId;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getPhotoUrl() {
        return mPhoto100;
    }




}
