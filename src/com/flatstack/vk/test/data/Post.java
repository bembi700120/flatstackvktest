package com.flatstack.vk.test.data;

import android.text.format.DateFormat;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public class Post {

    @SerializedName("text")
    private String mText;

    @SerializedName("date")
    private long mDate;

    @SerializedName("likes")
    private int mLikesCount;

    @SerializedName("reposts")
    private int mRepostsCount;

    @SerializedName("comments")
    private int mCommentsCount;

    @SerializedName("source_id")
    private int mSourceId;

    @SerializedName("group")
    private Group mGroup;

    @SerializedName("profile")
    private Profile mProfile;

    @SerializedName("photos")
    private ArrayList<Photo> mPhotoArray;


    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getDate() {
        return DateFormat.format("MM.dd.yyyy в HH:mm:ss", new Date(mDate * 1000)).toString();
    }

    public void setDate(long date) {
        mDate = date;
    }

    public int getLikesCount() {
        return mLikesCount;
    }

    public void setLikesCount(int count) {
        mLikesCount = count;
    }

    public void setRepostsCount(int count) {
        mRepostsCount = count;
    }

    public int getRepostsCount() {
        return mRepostsCount;
    }

    public void setCommentsCount(int count) {
        mCommentsCount = count;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setSourceId(int sourceId) {
        mSourceId = sourceId;
    }

    public boolean isGroupPost() {
        return mSourceId < 0;
    }

    public int getAbsSourceId() {
        return Math.abs(mSourceId);
    }

    public void setGroup(Group group) {
        mGroup = group;
    }

    public Group getGroup() {
        return mGroup;
    }

    public void setProfile(Profile profile) {
        mProfile = profile;
    }

    public Profile getProfile() {
        return mProfile;
    }

    public void setPhotos(ArrayList<Photo> photoArray) {
        mPhotoArray = photoArray;
    }

    public ArrayList<Photo> getPhotos() {
        return mPhotoArray;
    }
}
