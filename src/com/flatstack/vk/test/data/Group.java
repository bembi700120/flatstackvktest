package com.flatstack.vk.test.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public class Group {

    @SerializedName("name")
    private String mName;

    @SerializedName("photo_100")
    private String mPoster100;

    @SerializedName("id")
    private int mId;

    public int getGroupId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getPosterUrl() {
        return mPoster100;
    }
}
