package com.flatstack.vk.test.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class Photo {

    @SerializedName("photo_75")
    private String mPhoto75;

    @SerializedName("photo_130")
    private String mPhoto130;

    @SerializedName("photo_604")
    private String mPhoto604;

    @SerializedName("photo_807")
    private String mPhoto807;

    public String getPhoto75() {
        return mPhoto75;
    }

    public String getPhoto130() {
        return mPhoto130;
    }

    public String getPhoto604() {
        return mPhoto604;
    }

    public String getPhoto807() {
        return mPhoto807;
    }
}
