package com.flatstack.vk.test.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flatstack.vk.test.R;
import com.flatstack.vk.test.data.Photo;
import com.flatstack.vk.test.data.Post;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class PostDetailActivity extends Activity {

    public static final String INTENT_DATA = "data";
    private Post mPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActionBar();
        setContentView(R.layout.activity_posts_detail);
        mPost = new Gson().fromJson(getIntent().getStringExtra(INTENT_DATA), Post.class);
        ImageView poster = (ImageView) findViewById(R.id.poster);
        TextView sourceName = (TextView) findViewById(R.id.source_name);
        TextView date = (TextView) findViewById(R.id.date);
        TextView text = (TextView) findViewById(R.id.text);
        TextView commentsCount = (TextView) findViewById(R.id.comments_count);
        TextView repostsCount = (TextView) findViewById(R.id.reposts_count);
        TextView likeCount = (TextView) findViewById(R.id.like_count);
        LinearLayout photoContainer = (LinearLayout) findViewById(R.id.photoContainer);
        String posterUrl;
        String name;
        if (mPost.isGroupPost()) {
            posterUrl = mPost.getGroup().getPosterUrl();
            name = mPost.getGroup().getName();
        } else {
            posterUrl = mPost.getProfile().getPhotoUrl();
            name = mPost.getProfile().getFirstName() + " " + mPost.getProfile().getLastName();
        }
        Picasso.with(this).load(posterUrl).into(poster);
        sourceName.setText(name);
        date.setText(mPost.getDate());
        if (mPost.getText() == null || mPost.getText().trim().length() == 0) {
            text.setVisibility(View.GONE);
        } else {
            text.setText(mPost.getText());
        }
        if (mPost.getCommentsCount() == 0) {
            commentsCount.setText("");
        } else {
            commentsCount.setText(String.valueOf(mPost.getCommentsCount()));
        }
        if (mPost.getRepostsCount() == 0) {
            repostsCount.setText("");
        } else {
            repostsCount.setText(String.valueOf(mPost.getRepostsCount()));
        }
        if (mPost.getLikesCount() == 0) {
            likeCount.setText("");
        } else {
            likeCount.setText(String.valueOf(mPost.getLikesCount()));
        }
        if (mPost.getPhotos() != null) {
            for (Photo p : mPost.getPhotos()) {
                ImageView imageView = new ImageView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT, 1F);
                imageView.setLayoutParams(layoutParams);
                photoContainer.addView(imageView);
                Picasso.with(this).load(p.getPhoto604()).into(imageView);
            }
        } else {
            photoContainer.setVisibility(View.GONE);
        }
    }

    private void initActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ab_app_icon);
        actionBar.setDisplayShowTitleEnabled(false);
    }
}
