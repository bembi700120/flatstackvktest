package com.flatstack.vk.test.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.flatstack.vk.test.BusProvider;
import com.flatstack.vk.test.R;
import com.flatstack.vk.test.db.Cache;
import com.flatstack.vk.test.events.LogoutEvent;
import com.flatstack.vk.test.fragments.PostsFragment;
import com.squareup.otto.Subscribe;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;

public class PostsActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.flatstack.vk.test.R.layout.activity_news);
        initActionBar();
        VKUIHelper.onCreate(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new PostsFragment())
                .commitAllowingStateLoss();

    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    private void initActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(R.drawable.ab_app_icon);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Subscribe
    public void onEvent(LogoutEvent event) {
        if (VKSdk.isLoggedIn()) {
            VKSdk.logout();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            new Cache(this).clear();
        }
    }
}
