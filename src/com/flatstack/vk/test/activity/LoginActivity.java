package com.flatstack.vk.test.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.vk.sdk.*;
import com.vk.sdk.api.VKError;


public class LoginActivity extends FragmentActivity {

    private static final String[] sMyScope = new String[] {
            VKScope.OFFLINE,
            VKScope.FRIENDS,
            VKScope.WALL,
            VKScope.PHOTOS,
            VKScope.NOHTTPS
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.flatstack.vk.test.R.layout.activity_login);
        VKUIHelper.onCreate(this);
        VKSdk.initialize(sdkListener, "4496858");
        if (VKSdk.wakeUpSession()) {
            openNewsActivity();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        if (VKSdk.isLoggedIn()) {
            openNewsActivity();
        } else {
            VKSdk.authorize(sMyScope, true, false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    private final VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(sMyScope);
        }

        @Override
        public void onAccessDenied(final VKError authorizationError) {
            new AlertDialog.Builder(VKUIHelper.getTopActivity())
                    .setMessage(authorizationError.toString())
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            openNewsActivity();
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            openNewsActivity();
        }
    };

    private void openNewsActivity() {
        Intent intent = new Intent(this, PostsActivity.class);
        startActivity(intent);
        finish();
    }

}
