package com.flatstack.vk.test.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.flatstack.vk.test.data.Post;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class Cache extends SQLiteOpenHelper {

    private final static String DATA_BASE_NAME = "cache.db";
    private final static int DATA_BASE_VERSION = 1;
    private static final String TABLE_NAME = "cache";
    private static final String COLUMN_NAME = "post";

    public Cache(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE " + TABLE_NAME + " (" + COLUMN_NAME + " TEXT);";
        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public synchronized void add(ArrayList<Post> posts) {
        Gson gson = new Gson();
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        for (Post p : posts) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_NAME, gson.toJson(p));
            db.insert(TABLE_NAME, null, cv);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public synchronized ArrayList<Post> get() {
        Gson gson = new Gson();
        ArrayList<Post> data = new ArrayList<Post>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String json = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                cursor.moveToNext();
                data.add(gson.fromJson(json, Post.class));
            }
        }
        cursor.close();
        db.close();
        return data;
    }

    public synchronized void clear() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }
}
