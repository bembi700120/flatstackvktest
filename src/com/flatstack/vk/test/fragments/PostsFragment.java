package com.flatstack.vk.test.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.flatstack.vk.test.BusProvider;
import com.flatstack.vk.test.Pref;
import com.flatstack.vk.test.R;
import com.flatstack.vk.test.activity.PostDetailActivity;
import com.flatstack.vk.test.adapter.PostAdapter;
import com.flatstack.vk.test.api.GetLastPosts;
import com.flatstack.vk.test.api.GetMorePosts;
import com.flatstack.vk.test.data.Post;
import com.flatstack.vk.test.db.Cache;
import com.flatstack.vk.test.events.LogoutEvent;
import com.google.gson.Gson;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

import java.util.ArrayList;

/**
 * Created by Vitaly Kirillov on 07.08.2014.
 */
public class PostsFragment extends Fragment implements OnRefreshListener, GetLastPosts.OnGetLastPostsListener, GetMorePosts.OnGetMorePostsListener {

    private final static int POST_LIMIT = 25;
    private Activity mActivity;
    private PullToRefreshLayout mPullToRefreshLayout;
    private ListView mListView;
    private PostAdapter mListAdapter;
    private String mStartFrom;
    private boolean mBusy = false;
    private Cache mCache;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mCache = new Cache(activity);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragment = inflater.inflate(R.layout.fragment_posts, null);
        mPullToRefreshLayout = (PullToRefreshLayout) fragment.findViewById(R.id.pull_to_refresh);
        mListView = (ListView) fragment.findViewById(R.id.list_news);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount >= totalItemCount && !mBusy) {
                    mBusy = true;
                    new GetMorePosts(mStartFrom, POST_LIMIT, PostsFragment.this).execute();
                    mPullToRefreshLayout.setRefreshing(true);
                }
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Post p = mListAdapter.getItem(position);
                Intent intent = new Intent(mActivity, PostDetailActivity.class);
                intent.putExtra(PostDetailActivity.INTENT_DATA, new Gson().toJson(p));
                startActivity(intent);
            }
        });
        ActionBarPullToRefresh.from(getActivity())
                .theseChildrenArePullable(mListView)
                .listener(this)
                .setup(mPullToRefreshLayout);
        new AsyncTask<Void, Void, ArrayList<Post>>() {

            @Override
            protected ArrayList<Post> doInBackground(Void... params) {
                return mCache.get();
            }

            @Override
            protected void onPostExecute(ArrayList<Post> posts) {
                super.onPostExecute(posts);
                if (posts != null && posts.size() != 0) {
                    if (mListAdapter == null) {
                        mListAdapter = new PostAdapter(mActivity, posts);
                        mListView.setAdapter(mListAdapter);
                    }
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        mStartFrom = Pref.getLastStartFrom(mActivity);
        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mStartFrom != null)
            Pref.saveLastStartFrom(mActivity, mStartFrom);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.fragment_news, menu);
    }

    @Override
    public void onRefreshStarted(View view) {
        mBusy = true;
        new GetLastPosts(POST_LIMIT, this).execute();
        mPullToRefreshLayout.setRefreshing(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                BusProvider.getInstance().post(new LogoutEvent());
                return true;
        }
        return false;
    }

    @Override
    public void onGetLastPostsComplete(ArrayList<Post> posts, String startFrom) {
        mStartFrom = startFrom;
        if (mListAdapter == null) {
            mListAdapter = new PostAdapter(mActivity, posts);
            mListView.setAdapter(mListAdapter);
        } else {
            mListAdapter.clear();
            mListAdapter.addAll(posts);
            mListAdapter.notifyDataSetChanged();
        }
        addToCache(posts);
        mPullToRefreshLayout.setRefreshComplete();
        mBusy = false;
    }

    @Override
    public void onGetLastPostsError() {
        handleError();
    }

    @Override
    public void onGetMorePostsComplete(ArrayList<Post> posts, String startFrom) {
        mStartFrom = startFrom;
        mListAdapter.addAll(posts);
        mListAdapter.notifyDataSetChanged();
        mPullToRefreshLayout.setRefreshComplete();
        mBusy = false;
    }

    @Override
    public void onGetMorePostsError() {
        handleError();
    }

    private void handleError() {
        mBusy = false;
        mPullToRefreshLayout.setRefreshing(false);
    }

    private void addToCache(final ArrayList<Post> posts) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCache.clear();
                mCache.add(posts);
            }
        }).start();
    }
}
