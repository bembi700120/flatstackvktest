package com.flatstack.vk.test.api;

import com.flatstack.vk.test.LogCat;
import com.flatstack.vk.test.PostsParser;
import com.flatstack.vk.test.data.Post;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public class GetLastPosts extends AbsVkRequest{

    public interface OnGetLastPostsListener {
        public void onGetLastPostsComplete(ArrayList<Post> posts, String startFrom);
        public void onGetLastPostsError();
    }

    private int mLimit;
    private VKRequest mRequest;
    private OnGetLastPostsListener mListener;

    public GetLastPosts(int limit, OnGetLastPostsListener listener) {
        mLimit = limit;
        mListener = listener;
    }

    @Override
    public void execute() {
        mRequest = new VKRequest("newsfeed.get", VKParameters.from("count", String.valueOf(mLimit), "filters", "post"));

        mRequest.attempts = 3;
        mRequest.executeWithListener(this);
    }

    @Override
    public void onComplete(VKResponse vkResponse) {
        LogCat.d("onGetLastPostsComplete");
        try {
            PostsParser parser = new PostsParser(vkResponse);
            ArrayList<Post> posts = parser.getPosts();
            String nextFrom = parser.getNextFrom();
            mListener.onGetLastPostsComplete(posts, nextFrom);
            System.out.println();
        } catch (JSONException e) {
            mListener.onGetLastPostsError();
            LogCat.e(e);
        }
    }

    @Override
    public void onError(VKError error) {
        LogCat.d("onGetLastPostsError");
        mListener.onGetLastPostsError();
    }
}
