package com.flatstack.vk.test.api;

import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public abstract class AbsVkRequest extends VKRequest.VKRequestListener {

    public abstract void execute();

    public abstract void onComplete(VKResponse response);

    public abstract void onError(VKError error);
}
