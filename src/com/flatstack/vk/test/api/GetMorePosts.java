package com.flatstack.vk.test.api;

import com.flatstack.vk.test.LogCat;
import com.flatstack.vk.test.PostsParser;
import com.flatstack.vk.test.data.Post;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class GetMorePosts extends AbsVkRequest {

    public interface OnGetMorePostsListener {
        public void onGetMorePostsComplete(ArrayList<Post> posts, String startFrom);
        public void onGetMorePostsError();
    }

    private VKRequest mRequest;
    private String mStartFrom;
    private int mLimit;
    private OnGetMorePostsListener mListener;

    public GetMorePosts(String startFrom, int limit, OnGetMorePostsListener listener) {
        mStartFrom = startFrom;
        mLimit = limit;
        mListener = listener;
    }

    @Override
    public void execute() {
        mRequest = new VKRequest("newsfeed.get", VKParameters.from("count", String.valueOf(mLimit), "filters", "post",
                "start_from", mStartFrom));
        mRequest.attempts = 3;
        mRequest.executeWithListener(this);
    }

    @Override
    public void onComplete(VKResponse vkResponse) {
        LogCat.d("onGetMorePostsComplete");
        try {
            PostsParser parser = new PostsParser(vkResponse);
            ArrayList<Post> posts = parser.getPosts();
            String nextFrom = parser.getNextFrom();
            mListener.onGetMorePostsComplete(posts, nextFrom);
            System.out.println();
        } catch (JSONException e) {
            mListener.onGetMorePostsError();
            LogCat.e(e);
        }
    }

    @Override
    public void onError(VKError error) {
        LogCat.d("onGetMorePostsError");
        mListener.onGetMorePostsError();
    }
}
