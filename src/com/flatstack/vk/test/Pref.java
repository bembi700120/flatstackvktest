package com.flatstack.vk.test;

import android.content.Context;

/**
 * Created by Vitaly Kirillov on 10.08.2014.
 */
public class Pref {

    private static final String PREFERENCE_FILE = "prefs";
    private static final String START_FROM = "start_from";

    public static void saveLastStartFrom(Context context, String startFrom) {
        context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE)
                .edit()
                .putString(START_FROM, startFrom)
                .apply();
    }

    public static String getLastStartFrom(Context context) {
        return context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE)
                .getString(START_FROM, null);
    }
}
