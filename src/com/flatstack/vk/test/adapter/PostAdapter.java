package com.flatstack.vk.test.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flatstack.vk.test.R;
import com.flatstack.vk.test.data.Photo;
import com.flatstack.vk.test.data.Post;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Vitaly Kirillov on 08.08.2014.
 */
public class PostAdapter extends ArrayAdapter<Post>{

    private ArrayList<Post> mPosts;
    private LayoutInflater mLayoutInflater;

    public PostAdapter(Context context, ArrayList<Post> posts) {
        super(context, R.layout.fragment_posts_item);
        mLayoutInflater = LayoutInflater.from(context);
        mPosts = posts;
    }

    @Override
    public int getCount() {
        return mPosts.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Post post = mPosts.get(position);
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.fragment_posts_item, null);
            holder = new ViewHolder();
            holder.poster = (ImageView) convertView.findViewById(R.id.poster);
            holder.sourceName = (TextView) convertView.findViewById(R.id.source_name);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.showFullText = (TextView) convertView.findViewById(R.id.show_full);
            holder.commentsCount = (TextView) convertView.findViewById(R.id.comments_count);
            holder.repostsCount = (TextView) convertView.findViewById(R.id.reposts_count);
            holder.likeCount = (TextView) convertView.findViewById(R.id.like_count);
            holder.photoContainer = (LinearLayout) convertView.findViewById(R.id.photoContainer);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String posterUrl;
        String sourceName;
        if (post.isGroupPost()) {
            posterUrl = post.getGroup().getPosterUrl();
            sourceName = post.getGroup().getName();
        } else {
            posterUrl = post.getProfile().getPhotoUrl();
            sourceName = post.getProfile().getFirstName() + " " + post.getProfile().getLastName();
        }
        Picasso.with(getContext()).load(posterUrl).into(holder.poster);
        holder.sourceName.setText(sourceName);
        holder.date.setText(post.getDate());
        if (post.getText() == null || post.getText().trim().length() == 0) {
            holder.text.setVisibility(View.GONE);
            holder.showFullText.setVisibility(View.GONE);
        } else {
            holder.text.setVisibility(View.VISIBLE);
            holder.text.setText(post.getText());
            holder.text.post(new Runnable() {
                @Override
                public void run() {
                    if (holder.text.getLayout().getLineCount() > 5)
                        holder.showFullText.setVisibility(View.VISIBLE);
                    else
                        holder.showFullText.setVisibility(View.GONE);

                }
            });
        }
        if (post.getCommentsCount() == 0) {
            holder.commentsCount.setText("");
        } else {
            holder.commentsCount.setText(String.valueOf(post.getCommentsCount()));
        }
        if (post.getRepostsCount() == 0) {
            holder.repostsCount.setText("");
        } else {
            holder.repostsCount.setText(String.valueOf(post.getRepostsCount()));
        }
        if (post.getLikesCount() == 0) {
            holder.likeCount.setText("");
        } else {
            holder.likeCount.setText(String.valueOf(post.getLikesCount()));
        }
        holder.photoContainer.removeAllViews();
        if (post.getPhotos() != null) {
            holder.photoContainer.setVisibility(View.VISIBLE);
            int length = post.getPhotos().size() > 3 ? 3 : post.getPhotos().size();
            for (int i = 0; i < length; i++) {
                Photo photo = post.getPhotos().get(i);
                ImageView imageView = new ImageView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT, 1F);
                imageView.setLayoutParams(layoutParams);
                holder.photoContainer.addView(imageView);
                Picasso.with(getContext()).load(photo.getPhoto604()).into(imageView);
            }
        } else {
            holder.photoContainer.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public Post getItem(int position) {
        return mPosts.get(position);
    }

    @Override
    public void clear() {
        mPosts.clear();
    }

    @Override
    public void addAll(Collection<? extends Post> collection) {
        mPosts.addAll(collection);
    }

    static class ViewHolder {
        ImageView poster;
        TextView date;
        TextView sourceName;
        TextView text;
        TextView showFullText;
        TextView commentsCount;
        TextView repostsCount;
        TextView likeCount;
        LinearLayout photoContainer;
    }
}
