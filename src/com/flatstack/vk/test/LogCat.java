package com.flatstack.vk.test;

import android.util.Log;

public class LogCat {

    private static final String TAG = "flatstack";
    private static  final boolean sEnabled = true;

    private static boolean sShowSimpleClassName = true;
    private static boolean sShowMethodName = true;
    private static boolean sShowLineNumber = true;


    public static void v(Object value) {
        if (sEnabled) Log.v(TAG, getLocation() + String.valueOf(value));
    }

    public static void d(Object value) {
        Log.d(TAG, getLocation() + value);
    }

    public static void i(Object value) {
        if (sEnabled) Log.i(TAG, getLocation() + String.valueOf(value));
    }

    public static void w(Object value) {
        if (sEnabled) Log.w(TAG, getLocation() + String.valueOf(value));
    }

    public static void v(Object key, Object value) {
        if (sEnabled) Log.v(TAG, getLocation() + String.valueOf(key) + " = " + String.valueOf(value));
    }

    public static void d(Object key, Object value) {
        if (sEnabled) Log.d(TAG, getLocation() + String.valueOf(key) + " = " + String.valueOf(value));
    }

    public static void i(Object key, Object value) {
        if (sEnabled) Log.i(TAG, getLocation() + String.valueOf(key) + " = " + String.valueOf(value));
    }

    public static void w(Object key, Object value) {
        if (sEnabled) Log.w(TAG, getLocation() + String.valueOf(key) + " = " + String.valueOf(value));
    }

    public static void e(Exception value) {
        if (sEnabled) Log.e(TAG, getLocation(), value);
    }

    private static String getLocation() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        StringBuilder builder = new StringBuilder();
        if (sShowSimpleClassName) {
            builder.append(extractSimpleClassName(ste.getClassName()));
        } else {
            builder.append(ste.getClassName());
        }
        if (sShowMethodName) {
            builder.append(".").append(ste.getMethodName());
        }
        if (sShowLineNumber) {
            builder.append("(").append(ste.getLineNumber()).append(")");
        } else {
            builder.append("(").append(")");
        }
        builder.append(": ");
        return builder.toString();
    }

    private static String extractSimpleClassName(String fullClassName) {
        if ((null == fullClassName) || ("".equals(fullClassName)))
            return "";
        int lastDot = fullClassName.lastIndexOf('.');
        if (0 > lastDot) return fullClassName;
        return fullClassName.substring(++lastDot);
    }

    public static void showSimpleClassName(boolean show) {
        sShowSimpleClassName = show;
    }

    public static void showMethodName(boolean show) {
        sShowMethodName = show;
    }

    public static void showLineNumber(boolean show) {
        sShowLineNumber = show;
    }
}
